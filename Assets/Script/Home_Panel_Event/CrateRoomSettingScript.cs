﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;
using System;
using LitJson;
using UnityEngine.SceneManagement;


public class CrateRoomSettingScript : MonoBehaviour {

    public GameObject panelLYGSetting;
   // public GameObject panelZhuanzhuanSetting;
	//public GameObject panelChangshaSetting;
	//public GameObject panelHuashuiSetting;
	//public GameObject panelDevoloping;

	public List<Toggle> LYGRoomCards;//连云港麻将房卡数
	//public List<Toggle> changshaRoomCards;//长沙麻将房卡数
	//public List<Toggle> huashuiRoomCards;//划水麻将房卡数

	public List<Toggle> LYGGameRule;//连云港麻将玩法
                                    //public List<Toggle> changshaGameRule;//长沙麻将玩法
                                    //public List<Toggle> huashuiGameRule;//划水麻将玩法

    //public List<Toggle> LYGZhuama;//转转麻将抓码个数
    //public List<Toggle> changshaZhuama;//长沙麻将抓码个数
    //public List<Toggle> huashuixiayu;//划水麻将下鱼条数


    private int roomCardCount;//房卡数
	private GameObject gameSence;
	private RoomCreateVo sendVo;//创建房间的信息
	void Start () {
        //panelLYGSetting.SetActive(true);
        //panelZhuanzhuanSetting.SetActive (false);
		//panelChangshaSetting.SetActive (false);
		//panelHuashuiSetting.SetActive (false);
		//panelDevoloping.SetActive (false);
        openLYGSeetingPanel();

        SocketEventHandle.getInstance ().CreateRoomCallBack += onCreateRoomCallback;

	}
	
	// Update is called once per frame
	void Update () {
	
	}



    /***
	 * 打开LYG麻将设置面板
	 */
    public void openLYGSeetingPanel()
    {
        panelLYGSetting.SetActive(true);

        //panelZhuanzhuanSetting.SetActive(false);
        //panelChangshaSetting.SetActive(false);
        //panelHuashuiSetting.SetActive(false);
        //panelDevoloping.SetActive(false);
       
    }

    /***
	 * 打开转转麻将设置面板
	 */
    public void openZhuanzhuanSeetingPanel(){
        panelLYGSetting.SetActive(false);

        //panelZhuanzhuanSetting.SetActive (false);
		//panelChangshaSetting.SetActive (false);
		//panelHuashuiSetting.SetActive (false);
		//panelDevoloping.SetActive (true);
	
	}

	/***
	 * 打开长沙麻将设置面板
	 */ 
	public void openChangshaSeetingPanel(){
        panelLYGSetting.SetActive(false);

        //panelZhuanzhuanSetting.SetActive (false);
		//panelChangshaSetting.SetActive (false);
		//panelHuashuiSetting.SetActive (false);
		//panelDevoloping.SetActive (true);
	}

	/***
	 * 打开划水麻将设置面板
	 */ 
	public void openHuashuiSeetingPanel(){
        panelLYGSetting.SetActive(false);
       // panelZhuanzhuanSetting.SetActive (false);
		//panelChangshaSetting.SetActive (false);
		//panelHuashuiSetting.SetActive (false);
		//panelDevoloping.SetActive (true);
	}

	public void openDevloping(){
        panelLYGSetting.SetActive(false);
        //panelZhuanzhuanSetting.SetActive (false);
		//panelChangshaSetting.SetActive (false);
		//panelHuashuiSetting.SetActive (false);
		//panelDevoloping.SetActive (true);
	}

	public void closeDialog(){
		MyDebug.Log ("closeDialog");
		SocketEventHandle.getInstance ().CreateRoomCallBack -= onCreateRoomCallback;
		Destroy (this);
		Destroy (gameObject);
	}

    /**
 * 创建连云港麻将房间
 */
    public void createLYGRoom()
    {

        int roundNumber = 4;//房卡数                       
        
        for (int i = 0; i < LYGRoomCards.Count; i++)
        {
            Toggle item = LYGRoomCards[i];
            if (item.isOn)
            {
                if (i == 0)
                {
                    roundNumber = 4;
                }
                else if (i == 1)
                {
                    roundNumber = 8;
                }
               
                break;
            }
        }

        sendVo = new RoomCreateVo();
		if (APIS.ceShiLogin) {
			sendVo.roundNumber = 2;
		} else {
			sendVo.roundNumber = roundNumber;
		}
       
        sendVo.roomType = GameConfig.GAME_TYPE_LIANYUNGANG;
        string sendmsgstr = JsonMapper.ToJson(sendVo);
        if (GlobalDataScript.loginResponseData.account.roomcard > 0)
        {
            CustomSocket.getInstance().sendMsg(new CreateRoomRequest(sendmsgstr));
        }
        else
        {
            TipsManagerScript.getInstance().setTips("你的房卡数量不足，不能创建房间");
        }


    }

    

	



//	public void toggleHongClick(){
//
//		if (zhuanzhuanGameRule [2].isOn) {
//			zhuanzhuanGameRule [0].isOn = true;
//		}
//	}
//
//	public void toggleQiangGangHuClick(){
//		if (zhuanzhuanGameRule [1].isOn) {
//			zhuanzhuanGameRule [2].isOn = false;
//		}
//	}

	public void onCreateRoomCallback(ClientResponse response){
		MyDebug.Log (response.message);
		if (response.status == 1) {
			
			//RoomCreateResponseVo responseVO = JsonMapper.ToObject<RoomCreateResponseVo> (response.message);           
            int roomid = Int32.Parse(response.message);
         
            sendVo.roomId = roomid;
            //sendVo.huaList= responseVO.huaList;
            //sendVo.cardCount = responseVO.cardCount;

            GlobalDataScript.roomVo = sendVo;
			GlobalDataScript.loginResponseData.roomId = roomid;
			//GlobalDataScript.loginResponseData.isReady = true;
			GlobalDataScript.loginResponseData.main = true;
			GlobalDataScript.loginResponseData.isOnLine = true;

			//SceneManager.LoadSceneAsync(1);
			/**
			if (gameSence == null) {
				gameSence = Instantiate (Resources.Load ("Prefab/Panel_GamePlay")) as GameObject;
				gameSence.transform.parent = GlobalDataScript.getInstance ().canvsTransfrom;
				gameSence.transform.localScale = Vector3.one;
				gameSence.GetComponent<RectTransform> ().offsetMax = new Vector2 (0f, 0f);
				gameSence.GetComponent<RectTransform> ().offsetMin = new Vector2 (0f, 0f);
				gameSence.GetComponent<MyMahjongScript> ().createRoomAddAvatarVO (GlobalDataScript.loginResponseData);
			}*/
			GlobalDataScript.gamePlayPanel = PrefabManage.loadPerfab ("Prefab/Panel_GamePlay");

			GlobalDataScript.gamePlayPanel.GetComponent<MyMahjongScript> ().createRoomAddAvatarVO (GlobalDataScript.loginResponseData);
		
			closeDialog ();		

		} else {
			TipsManagerScript.getInstance ().setTips (response.message);
		}
	}

}
