﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[Serializable]

	public class HupaiResponseItem
	{
		public int[]paiArray;
		public TotalInfo totalInfo;
		public string huType;
		public string nickname;
		public int gangScore;
		public int totalScore;
		public int uuid;
		private string icon;
		private List<int> maPonits;
        public int huaCount;
        public int sumScore;
        public bool baoTing;

        public HupaiResponseItem ()
		{
		}

		public void setMaPoints(List<int> mas){
			maPonits = mas;
		}
		public List<int> getMaPoints(){
			return  maPonits;
		}

		public void setIcon(string iconFlag){
			icon = iconFlag;
		}

		public string getIcon(){
			return icon;
		}

		public void setNickName(string nickName) {
			this.nickname = nickName;
		}

		public string getNickName() {
			return this.nickname;
		}
	}
}

