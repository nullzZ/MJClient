﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[Serializable]
	public class RoomCreateVo
	{
		public  bool hong;
		public int ma;
		public int roomId;
		public int roomType;//1；连云港2、划水；3、长沙
		/**局数**/
		public int roundNumber;
		public bool sevenDouble;
		public int ziMo;//1：自摸胡；2、抢杠胡
		public int xiaYu;
		public string name;
		public bool addWordCard;
		public int magnification;
		/**
		* 牌的总个数
		*/
		public int cardCount;

		/**
	 	* 花牌
	 	*/
		public List<int> huaList;
		public RoomCreateVo()
		{

		}
	}
}

