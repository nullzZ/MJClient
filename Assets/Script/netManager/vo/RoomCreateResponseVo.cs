﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[Serializable]
	public class RoomCreateResponseVo
	{
		public int roomId;
        /**
 * 牌的总个数
 */
        public int cardCount;

        /**
	 * 花牌
	 */
        public List<int> huaList;

        public RoomCreateResponseVo()
		{
		}
	}
}

