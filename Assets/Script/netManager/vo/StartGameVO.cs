﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[Serializable]
	public class StartGameVO
	{
		public List<List<int>> paiArray;
		public int bankerId;
		public List<int> huaList0;
		public List<int> huaList1;
		public List<int> huaList2;
		public List<int> huaList3;
        public List<TingVo> tings;
        public string buttons;


        public StartGameVO ()
		{
		}
	}
}

