﻿using System;

namespace AssemblyCSharp
{
	[Serializable]
	public class ServiceVersionVo
	{
		public Version123 ios;
		public Version123 Android;
		public Version123 Windows;
		public ServiceVersionVo ()
		{
			
		}
	}
}

