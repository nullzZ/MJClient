﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	[Serializable]
	public class OtherPengGangBackVO
	{
		public int cardPoint;
		public int avatarId;
        public List<TingVo> tings;

        public OtherPengGangBackVO ()
		{
		}
	}
}

