﻿using System;
using LitJson;

namespace AssemblyCSharp
{
	public class ReConnectScript
	{
		private static ReConnectScript _instance;

		public static ReConnectScript getInstance ()
		{
			if (_instance == null) {
				_instance = new ReConnectScript ();
				//_instance.Connect ();
			}
			return _instance;
		}

		public ReConnectScript ()
		{
		}

		public void ReConnectToServer ()
		{
			//CustomSocket.getInstance ().Connect ();
			if (!GlobalDataScript.isonLoginPage) {
				MyDebug.Log ("重新登录1");
				if (GlobalDataScript.loginResponseData != null) {
					MyDebug.Log ("重新登录2");
					//SocketEventHandle.getInstance ().LoginCallBack += SocketEventHandle.getInstance ().LoginCallBack;
					//SocketEventHandle.getInstance ().RoomBackResponse += SocketEventHandle.getInstance ().RoomBackResponse;
					LoginVo loginvo = new LoginVo();
					loginvo.openId = GlobalDataScript.loginResponseData.account.openid;
					loginvo.nickName = GlobalDataScript.loginResponseData.account.nickname;
					loginvo.headIcon = GlobalDataScript.loginResponseData.account.headicon;
					loginvo.unionid = GlobalDataScript.loginResponseData.account.unionid;
					loginvo.province = GlobalDataScript.loginResponseData.account.province;
					loginvo.city = GlobalDataScript.loginResponseData.account.city;
					loginvo.sex = GlobalDataScript.loginResponseData.account.sex;
					loginvo.IP = GlobalDataScript.getInstance().getIpAddress();
					String msg = JsonMapper.ToJson(loginvo);
					CustomSocket.getInstance ().sendMsg (new LoginRequest (msg));
				}
			}
		}


	}
}


