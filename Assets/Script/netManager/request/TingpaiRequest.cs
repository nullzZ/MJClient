﻿using System;

namespace AssemblyCSharp
{
	public class TingPaiRequest:ClientRequest
	{
		
		public TingPaiRequest(string sendMsg)
		{
			headCode = APIS.TING_REQUEST;
			messageContent = sendMsg;
		}
	}
}

