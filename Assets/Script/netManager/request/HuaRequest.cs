﻿using System;
using LitJson;

namespace AssemblyCSharp
{
	public class HuaRequest:ClientRequest
	{

		public HuaRequest(CardVO cardvo)
		{
			headCode = APIS.HUA_REQUEST;
			messageContent = JsonMapper.ToJson (cardvo);
		}
	}
}
