﻿using UnityEngine;
using System.Collections;
using System;


//时间和电量
public class BatteryAndTime : MonoBehaviour
{
    string _time = string.Empty;
    string _battery = string.Empty;

    void Start()
    {
        StartCoroutine("UpdataTime");
        StartCoroutine("UpdataBattery");

		if(Application.internetReachability == NetworkReachability.NotReachable){
			Debug.Log("网络不可达");
		}

		if(Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork){
			Debug.Log("通过运营商数据网络可达");
		}

		if(Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork){
			Debug.Log("通过局域网络可达(wifi)");
		}

		//NetworkReachability.NotReachable;//网络不可达

		//NetworkReachability.ReachableViaCarrierDataNetwork;//通过运营商数据网络可达

		//NetworkReachability.ReachableViaLocalAreaNetwork; //通过局域网络可达(wifi)

    }

    void OnGUI()
    {
        GUILayout.Label(_time, GUILayout.Width(100), GUILayout.Height(100));
        GUILayout.Label(_battery, GUILayout.Width(100), GUILayout.Height(100));
    }

    IEnumerator UpdataTime()
    {
        DateTime now = DateTime.Now;
        _time = string.Format("{0}:{1}", now.Hour, now.Minute);
        yield return new WaitForSeconds(60f - now.Second);
        while (true)
        {
            now = DateTime.Now;
            _time = string.Format("{0}:{1}", now.Hour, now.Minute);
            yield return new WaitForSeconds(60f);
        }
    }

    IEnumerator UpdataBattery()
    {
        while (true)
        {
            _battery = GetBatteryLevel().ToString();
            yield return new WaitForSeconds(300f);
        }
    }

    int GetBatteryLevel()
    {
        try
        {
            string CapacityString = System.IO.File.ReadAllText("/sys/class/power_supply/battery/capacity");
            return int.Parse(CapacityString);
        }
        catch (Exception e)
        {
            Debug.Log("Failed to read battery power; " + e.Message);
        }
        return -1;
    }
}
