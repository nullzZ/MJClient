﻿using UnityEngine;
using System.Collections;

//loading
public class ObjRotate : MonoBehaviour
{
	public float speed = 350f;
	bool isRotate = false;

	public void Show ()
	{
		if(!isRotate){
			Quaternion que = new Quaternion ();
			que.eulerAngles = Vector3.zero;
			gameObject.transform.rotation = que;
		}
		if(isRotate){
			return;
		}
		isRotate = true;
		gameObject.SetActive (true);
	}


	public void Conceal ()
	{
		if(!isRotate){
			return;
		}
		isRotate = false;
		gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update ()
	{
		if (isRotate)
			gameObject.transform.Rotate (-Vector3.forward * Time.deltaTime * speed);
	}
}
