﻿using UnityEngine;
using System.Collections;

public class LogOutputHandler : MonoBehaviour
{

	//Register the HandleLog function on scene start to fire on debug.log events
	public void OnEnable()
	{
		Application.logMessageReceived += HandleLog;
	}

	//Remove callback when object goes out of scope
	public void OnDisable()
	{
		Application.logMessageReceived -= HandleLog;
	}

	string project = "kxmh-unity";
	string logstore = "kxmh";
	string serviceAddr = "kxmh-unity.cn-shenzhen.log.aliyuncs.com";

	//Capture debug.log output, send logs to Loggly
	public void HandleLog(string logString, string stackTrace, LogType type)
	{
		string parameters = "";
		parameters += "Level=" + WWW.EscapeURL(type.ToString());
		parameters += "&";
		parameters += "Message=" + WWW.EscapeURL(logString);
		parameters += "&";
		parameters += "Stack_Trace=" + WWW.EscapeURL(stackTrace);
		parameters += "&";
		//Add any User, Game, or Device MetaData that would be useful to finding issues later
		parameters += "Device_Model=" + WWW.EscapeURL(SystemInfo.deviceModel);

		string url = "http://" + project + "." + serviceAddr + "/logstores/" + logstore + "/track?APIVersion=0.6.0&" + parameters;
		StartCoroutine(SendData(url));
	}

	public IEnumerator SendData(string url)
	{
		WWW sendLog = new WWW(url);
		yield return sendLog;
	}
}
